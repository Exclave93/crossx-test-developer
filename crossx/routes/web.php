<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContatoController;
use App\Http\Controllers\ContatoNumeroController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

/* SYSTEM ROUTES*/
Route::middleware(['auth:sanctum', 'verified'])->get('/contato', [ContatoController::class, 'index']);

/* API ROUTES */
Route::middleware(['auth:sanctum', 'verified'])->get('/contato/add', [ContatoController::class, 'create']);
Route::middleware(['auth:sanctum', 'verified'])->get('/contatos', [ContatoController::class, 'showAll']);
Route::middleware(['auth:sanctum', 'verified'])->post('/contato', [ContatoController::class, 'store']);
Route::middleware(['auth:sanctum', 'verified'])->get('/contato/{id}', [ContatoController::class, 'show']);
Route::middleware(['auth:sanctum', 'verified'])->get('/contato/edit/{id}', [ContatoController::class, 'edit']);
Route::middleware(['auth:sanctum', 'verified'])->post('/contato/{id}', [ContatoController::class, 'update']);
Route::middleware(['auth:sanctum', 'verified'])->delete('/contato/{id}', [ContatoController::class, 'destroy']);
Route::middleware(['auth:sanctum', 'verified'])->post('/contatonumero', [ContatoNumeroController::class, 'store']);
Route::middleware(['auth:sanctum', 'verified'])->get('/contatonumeros/{id}', [ContatoNumeroController::class, 'show']);
Route::middleware(['auth:sanctum', 'verified'])->delete('/contatonumeros/{id}', [ContatoNumeroController::class, 'destroy']);