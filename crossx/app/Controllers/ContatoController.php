<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contato;
use App\Http\Resources\ContatoResource;
use App\Http\Requests\ContatoRequest;

class ContatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = Contato::all()->sortBy('nome');

        $contatos = $collection->filter(function ($value, $key) {
            return $value->user_id == auth()->user()->id;
        });

        return view('contato.index', $contatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contato.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(ContatoRequest $request)
    {
        $contato = Contato::create($request->all());

        return $contato;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contato = Contato::find($id);

        return view('contato.show', ['data' => $contato]);
    }

    /**
     * Display all resource.
     *
     */
    public function showAll()
    {
        $collection = Contato::with('contatoNumeros')->get();
        
        $contatos = $collection->filter(function ($value, $key) {
            return $value->user_id == auth()->user()->id;
        });

        return ContatoResource::collection($contatos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contato = Contato::find($id);
        
        return view('contato.edit', ['data' => $contato]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContatoRequest $request, $id)
    {
        $contato = Contato::find($id);
        $contato->fill($request->all());
        $contato->update();

        return $contato;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contato::destroy($id);
        return redirect('contato');
    }
}
