<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContatoNumero extends Model
{
    use HasFactory;

    protected $fillable = [
        'numero', 'contato_id'
    ];
}
