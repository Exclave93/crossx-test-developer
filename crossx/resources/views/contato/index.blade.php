<style media="screen">
    .img-avatar-xs {
        width: 30px;
        height: 30px;
        border: 1px solid #c0c0c0;
        border-radius: 5px;
    }

    .a-line {
        line-height: 30px;
    }
</style>

<meta name="user-id" content="{{ Auth::user()->id }}">

<div id="app">
    <front-page>

    </front-page>
</div>

<script src="{{ asset('js/app.js') }}" async defer></script>